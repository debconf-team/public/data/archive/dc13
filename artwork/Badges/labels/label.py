#!/usr/bin/env python3

# Usage:
#
#   ./label.py { SEARCH_STRING | ID }
#
# Set DRY_RUN=1 to no actually print.
# Set PRINTER_NAME to the name of the target printer, that will be passed as
# `lp -d $PRINTER_NAME', unless you're happy with the default one.

import sys
import csv
import os
import re
import tempfile
import subprocess
import shutil
import os

csv_file = "penta-data.csv"
template_file = "label-template.tex"
printer_name = os.getenv('PRINTER_NAME', 'DC13_QL580N_Label_Printer')

# Get hint
if len(sys.argv) < 2:
    sys.exit('Usage: %s search_string' % sys.argv[0])

dry_run = os.getenv('DRY_RUN', '0')
if dry_run != '0':
    dry_run = True
else:
    dry_run = False

search_string = sys.argv[1].lower()

# person_id,name,nickname,debconf_role,food,food_select,language
csv_id = 0
csv_fullname = 1
csv_nickname = 2
csv_dcrole = 3
csv_foodspec = 4
csv_foodpaid = 5
csv_languages = 6

def build_attendees_by_id(csv_file):
    attendees_reader = csv.reader(csvfile)
    att = {}
    for attendee in attendees_reader:
        att[attendee[csv_id]] = attendee
    return att

def looks_like_an_attendee_id(search_string):
    if re.match('[0-9]+$', search_string):
        return True
    else:
        return False

def is_known_attendee_id(search_string, attendees):
    if search_string in attendees:
        return True
    return False

with open(csv_file, 'r') as csvfile:
    attendees = build_attendees_by_id(csvfile)

    if looks_like_an_attendee_id(search_string):
        if is_known_attendee_id(search_string, attendees):
            print('%s is a known attendee id.' % search_string)
            id_to_print = search_string
        else:
            raise IndexError("'%s' is not a known attendee id" % search_string)
    else:
        print("Attendees matching '%s'" % search_string)
        print("")
    
        for attendee_id in attendees:
            attendee = attendees[attendee_id]
            fullname = attendee[csv_fullname]
            nickname = attendee[csv_nickname]
            if fullname.lower().startswith(search_string) or nickname.lower().startswith(search_string):
                print("%4d : %s (%s)" % (int(attendee[csv_id]), fullname, nickname))
        print("")
        id_to_print = input("ID of this attendee: ")

    # Check that the id is valid 
    if id_to_print not in attendees:
        print("%s is not a valid ID, please retry" % id_to_print)
    else:
        print("----")
        print('Nick: ' + attendees[id_to_print][csv_nickname])
        print('Name: ' + attendees[id_to_print][csv_fullname])
        print('Lang: ' + attendees[id_to_print][csv_languages])
        print('Food: ' + attendees[id_to_print][csv_foodspec])
        print('Paid: ' + attendees[id_to_print][csv_foodpaid])
        print('')
        print('Role: ' + attendees[id_to_print][csv_dcrole])
        print("----")
        input("Prepare the correct label in the printer, hit enter when done")
        print("Generating label for attendee ID %d …" % int(id_to_print))
    
        # Create temporary directory
        pdfgendir = tempfile.mkdtemp()
        
        try:
            # Create temporary filenames and directory
            [texfile_h, texfile] = tempfile.mkstemp(dir=pdfgendir)
            texfile_f = os.fdopen(texfile_h, "w")
            
            for line in open(template_file,'r'):
                if attendees[id_to_print][csv_nickname]:
                    line = line.replace('\\nickname',attendees[id_to_print][csv_nickname].replace('_','\\_'))
                else:
                    line = line.replace('\\nickname','~')
                line = line.replace('\\fullname',attendees[id_to_print][csv_fullname])
                line = line.replace('\\languages',attendees[id_to_print][csv_languages])
                line = line.replace('\\foodspec',attendees[id_to_print][csv_foodspec])
                if attendees[id_to_print][csv_foodpaid] == 'yes':
                    line = line.replace('\\foodpaid','✓')
                else:
                    line = line.replace('\\foodpaid','')
                texfile_f.write(line)
            texfile_f.close()
            
            # Generate label PDF
            subprocess.call(['xelatex','-output-directory',pdfgendir,texfile])
            # Somehow pdflatex can't be told a PDF name
            pdfname = texfile + ".pdf"

            # Display it
            #subprocess.call(['okular', pdfname], stdout=None)
            
            # Print it, twice (-n 2 prints 4 for some reason), unless $DRY_RUN=1
            if dry_run:
                print("Dry run, not printing.")
            else:
                print('Printing on %s...' % printer_name)
                subprocess.call(['lp','-d',printer_name,pdfname])
                subprocess.call(['lp','-d',printer_name,pdfname])

        except KeyboardInterrupt:
            print("oops")
        # Delete your trails
        shutil.rmtree(pdfgendir)

