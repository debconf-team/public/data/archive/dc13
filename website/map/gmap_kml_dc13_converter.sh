#!/bin/sh

set -e

infile=$1

cat $1	| sed -e 's#http://maps.gstatic.com/mapfiles/.*/bus.png#map/maki/bus-24.png#g;' \
	| sed -e 's#http://maps.gstatic.com/mapfiles/.*/rangerstation.png#map/maki/embassy-24.png#g;' \
	| sed -e 's#LeCamp#Le Camp#g;'
