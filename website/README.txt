Small documentation:

* You need libtemplate-perl package from sources.
* page.tt consits the general template
* strings.tt.en and strings.tt.LANGUAGECODE contain all strings that are used by page.tt
* Archives are static html inside of index.xhtml.LANGUAGECODE with a symbolic link from index.xhtml to the standard language document.
* Check and adjust directories in the ../bin/update-website script to test it on your local computer.
