# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR DebConf Organizer Team
# This file is distributed under the same license as the Cover Letter package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Cover Letter 0.1351425242\n"
"Report-Msgid-Bugs-To: debconf-team@lists.debconf.org\n"
"POT-Creation-Date: 2012-10-28 12:54+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#: cover-letter.en.tex:6
msgid "\\usepackage[english]{chextras}"
msgstr ""

#. type: Plain text
#: cover-letter.en.tex:6
msgid "\\usepackage[nodayofweek]{datetime}"
msgstr ""

#. type: Plain text
#: cover-letter.en.tex:10
msgid ""
"\\def\\DCsignatureappendix{\\\\\\footnotesize on behalf of the DebConf13 "
"organisation team}"
msgstr ""

#. type: title{#1}
#: cover-letter.en.tex:14
msgid "\\textbf{Sponsoring the 2013 Annual Debian Conference}"
msgstr ""

#. type: letter
#: cover-letter.en.tex:18
msgid "{\\targetName\\\\\\targetAddress}"
msgstr ""

#. type: letter
#: cover-letter.en.tex:20
msgid "\\object"
msgstr ""

#. type: letter
#: cover-letter.en.tex:22
msgid "\\opening{Dear \\toname,}"
msgstr ""

#. type: letter
#: cover-letter.en.tex:27
msgid ""
"I am writing on behalf of the organising committee of the 14th annual Debian "
"developers' conference, ``DebConf13''.  We would like to invite you to "
"consider the sponsorship opportunities available for this major Free "
"Software event."
msgstr ""

#. type: letter
#: cover-letter.en.tex:36
msgid ""
"DebConf is the Debian Project's developer conference.  In addition to a full "
"schedule of technical, social and policy talks, DebConf provides an "
"opportunity for developers, contributors and other interested people to meet "
"in person and work together intensely.  It has taken place annually since "
"2000 in locations as varied as Scotland, Argentina, and Bosnia and "
"Herzegovina.  In August 2013, DebConf will be held for the first time in "
"Switzerland, at ``Le Camp'' in Vaumarcus."
msgstr ""

#. type: letter
#: cover-letter.en.tex:44
msgid ""
"While not being an academic conference, DebConf has a close association with "
"the scientific community from many universities in Europe, the USA and Latin "
"America, since many contributors who volunteer their time to Debian are "
"based in universities.  The global impact of the event has risen over the "
"years thanks to continuous support from sponsors such as HP, Google, "
"Canonical, Intel, ARM, Telefonica, O'Reilly and Nokia."
msgstr ""

#. type: letter
#: cover-letter.en.tex:48
msgid ""
"We would be very grateful if you could find time to talk to us, so that we "
"could explain in detail the advantages and benefits that come from "
"supporting the event."
msgstr ""

#. type: letter
#: cover-letter.en.tex:52
msgid ""
"I have attached a document with more information on DebConf and on "
"sponsorship opportunities for this year's event.  Please don't hesitate to "
"get in touch with me if you have any questions."
msgstr ""

#. type: letter
#: cover-letter.en.tex:54
msgid "\\closing{Yours sincerely,}"
msgstr ""
