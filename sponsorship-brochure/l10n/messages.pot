# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR DebConf Team
# This file is distributed under the same license as the Sponsoring Brochure package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Sponsoring Brochure 0.1354729381\n"
"Report-Msgid-Bugs-To: debconf-team@lists.debconf.org\n"
"POT-Creation-Date: 2012-12-05 18:43+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#: brochure.en.tex:37
msgid ""
"\\usepackage[ pdfauthor={DebConf Team}, pdftitle={DebConf13 sponsoring "
"brochure}, pdfsubject={Sponsoring the 14th Debian annual Conference}, "
"hidelinks ]{hyperref}"
msgstr ""

#.  Standard margins; left is bigger because titles are "-1 cm"
#. type: Plain text
#: brochure.en.tex:40
msgid "\\usepackage[top=2cm,bottom=2cm,left=2.5cm,right=1.5cm]{geometry}"
msgstr ""

#. type: Plain text
#: brochure.en.tex:49
msgid "\\usepackage[english]{babel}"
msgstr ""

#. type: Plain text
#: brochure.en.tex:49
msgid "\\usepackage[utf8]{inputenc}"
msgstr ""

#. type: Plain text
#: brochure.en.tex:49
msgid "\\usepackage[T1]{fontenc}"
msgstr ""

#. type: Plain text
#: brochure.en.tex:49
msgid "\\usepackage{dejavu}"
msgstr ""

#. type: Plain text
#: brochure.en.tex:49
msgid "\\usepackage{fancyhdr}"
msgstr ""

#. % Better tables
#. type: Plain text
#: brochure.en.tex:52
msgid "\\usepackage{tabu}"
msgstr ""

#. % Wrapped figures
#. type: Plain text
#: brochure.en.tex:55
msgid "\\usepackage{wrapfig}"
msgstr ""

#. % Multiple columns
#. type: Plain text
#: brochure.en.tex:58
msgid "\\usepackage{multicol}"
msgstr ""

#. % Bottom shade
#. type: Plain text
#: brochure.en.tex:62
msgid "\\usepackage[all]{background}"
msgstr ""

#. type: Plain text
#: brochure.en.tex:62
msgid "\\usepackage{tikz}"
msgstr ""

#. % Coloured titles
#. type: Plain text
#: brochure.en.tex:66
msgid "\\usepackage{titlesec}"
msgstr ""

#. type: Plain text
#: brochure.en.tex:66
msgid "\\usepackage{color}"
msgstr ""

#. type: tikzpicture
#: brochure.en.tex:94
msgid ""
"\\shade[top color=white,bottom color=dcshade] (0,-\\paperheight) rectangle "
"(\\paperwidth,-\\paperheight+0.85\\headheight);"
msgstr ""

#. type: lfoot{#1}
#: brochure.en.tex:115
msgid "DebConf13 | Vaumarcus, Switzerland - August 5-18, 2013"
msgstr ""

#. % graphics support
#. type: Plain text
#: brochure.en.tex:128
msgid "\\usepackage{graphicx}"
msgstr ""

#. type: quote
#: brochure.en.tex:135
msgid "#1"
msgstr ""

#. type: flushright
#: brochure.en.tex:135
msgid "#2"
msgstr ""

#.  DD (man)
#. type: newcommand{#4}
#: brochure.en.tex:138
msgid "Debian Developer"
msgstr ""

#.  DD (woman)
#. type: newcommand{#4}
#: brochure.en.tex:141
msgid "Debian Developer~"
msgstr ""

#.  Added rule for word bénévole
#. type: hyphenation{#1}
#: brochure.en.tex:144
msgid "béné-vole"
msgstr ""

#.  Disable the red shade below
#. type: document
#: brochure.en.tex:151
msgid "\\NoBgThispage"
msgstr ""

#.  For the PDF outline
#. type: addcontentsline{#3}
#: brochure.en.tex:157
msgid "DebConf13 - Sponsoring brochure"
msgstr ""

#. type: center
#: brochure.en.tex:157 brochure.en.tex:163 brochure.en.tex:166
msgid "~"
msgstr ""

#. type: center
#: brochure.en.tex:157
msgid "[1cm]"
msgstr ""

#. type: textsc{#1}
#: brochure.en.tex:163
msgid "14th Annual Conference"
msgstr ""

#. type: center
#: brochure.en.tex:163 brochure.en.tex:166
msgid "[3cm]"
msgstr ""

#. type: huge
#: brochure.en.tex:170
msgid "Sponsoring Brochure"
msgstr ""

#. type: tikzpicture
#: brochure.en.tex:179
msgid ""
"\\node[above] at (current page.south) "
"{\\includegraphics[width=1.05\\paperwidth]{img/Dc13-leogg-mountains}};"
msgstr ""

#. type: section{#2}
#: brochure.en.tex:181
msgid "Debian"
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:183
msgid "Free Operating System"
msgstr ""

#. type: document
#: brochure.en.tex:190
msgid ""
"Debian is a free and open operating system, consisting entirely of Free and "
"Open Source Software. Its strict guidelines serve as an example for many "
"other projects. The Debian project is dedicated to remaining 100\\% free and "
"to being a responsible ``citizen'' of the Free and Open Source Software "
"ecosystem. Its key priorities are Free Software and its users."
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:200
msgid "Debian is Choice"
msgstr ""

#. type: document
#: brochure.en.tex:200
msgid ""
"Debian is known for its adherence to the Unix and Free Software "
"philosophies, and for its coverage -- the current release includes over "
"20,000 software packages for more than ten computer architectures, ranging "
"from the more common Intel 32-bit and 64-bit architectures to ARM "
"(cellphones and tablets) and the IBM s390 (mainframes). Besides the "
"well-known Linux kernel it also supports the FreeBSD kernel.  Debian is the "
"basis for several other distributions, including Knoppix and Ubuntu."
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:202
msgid "A Large Scale, Non-profit, Distributed Project"
msgstr ""

#. type: document
#: brochure.en.tex:219
msgid ""
"The Debian Project's key strengths are its volunteer base, its dedication to "
"the Debian Social Contract, and its commitment to providing the best "
"operating system possible. Debian is produced by over a thousand active "
"developers spread around the world, many of whom volunteer in their spare "
"time. Apart from DebConf most communication takes place on the Internet in "
"mailing-lists and IRC chat-rooms.  Unlike all other Linux distributions of "
"similar size Debian is an independent non-profit organization not backed by "
"a commercial company."
msgstr ""

#. type: document
#: brochure.en.tex:223
msgid ""
"Debian's dedication to Free Software, its non-profit nature, and its open "
"development model make it unique among Free Software distributions."
msgstr ""

#. type: section{#2}
#: brochure.en.tex:225
msgid "DebConf"
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:227
msgid "Debian Annual Conference"
msgstr ""

#. type: document
#: brochure.en.tex:234
msgid ""
"DebConf is the annual conference for Debian contributors and interested "
"users. Previous Debian Conferences have featured speakers and attendees from "
"around the world (DebConf11 had about 300 attendees from more than 50 "
"different countries). Thanks to our sponsors, \\textbf{participation, "
"accommodation, and meals are free of charge} to Debian developers and "
"contributors."
msgstr ""

#. type: document
#: brochure.en.tex:240
msgid ""
"DebConf13 will take place in August 2013 on the shore of Lake Neuchâtel in "
"Vaumarcus, Switzerland. The conference venue, ``Le Camp'', offers an "
"idyllic, yet affordable environment for informal workgroup meetings as well "
"as more formal discussion sessions and technical "
"presentations. Accommodation for most participants will be on-site."
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:245
msgid "Three distinct parts"
msgstr ""

#. type: textbf{#1}
#: brochure.en.tex:245
msgid "Sponsoring the Debian Conference"
msgstr ""

#. type: document
#: brochure.en.tex:245
msgid ""
"will help cover the costs of the Conference, as well as enable three "
"distinct sub-events:\\\\"
msgstr ""

#. type: document
#: brochure.en.tex:262
msgid ""
"{ \\tabulinesep=6pt \\begin{tabu} to \\linewidth{@{}X X X} "
"\\rowfont[c]{\\bfseries\\color{DebConf_tableheader}} DebConf & DebCamp & "
"Debian Day \\\\ Hands-on coding, workshops, and talks -- all highly "
"technical in nature.  & The week prior to the conference is reserved for "
"development teams to participate in undisturbed collaborative work.  & An "
"open day for users and decision makers to discover the benefits of Free "
"Software. \\\\ \\end{tabu} }"
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:262
msgid "Video"
msgstr ""

#. type: document
#: brochure.en.tex:268
msgid ""
"We have a dedicated team of volunteers recording all talks at DebConf.  The "
"sessions are streamed live via the Internet for remote participants. All "
"videos are also archived for the further benefit of the "
"community. \\textbf{We thank our Platinum and Gold sponsors by inserting "
"their logos in all video transmissions}."
msgstr ""

#. type: section{#2}
#: brochure.en.tex:270
msgid "DebConf and Debian"
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:272
msgid "The Bigger Picture"
msgstr ""

#. type: document
#: brochure.en.tex:276
msgid ""
"The benefits that DebConf brings to the Debian Project as a whole, and the "
"larger Free Software community, are best summarized by the following "
"comment:"
msgstr ""

#. type: DCquote{#1}
#: brochure.en.tex:286
msgid ""
"This is the kind of peer-to-peer networking that makes open source "
"conferences like DebConf6 worthwhile. It wasn't a teacher/student "
"relationship. It was two hackers sharing knowledge with each other. [...] "
"\\textbf{The knowledge they shared will, no doubt, improve Debian --- and by "
"extension will help improve GNU/Linux for everyone}, whether they use Debian "
"(or a Debian derivative), SuSE, Fedora, Gentoo, Mandriva, or any other "
"distribution."
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:288
msgid "Knowledge Sharing"
msgstr ""

#. type: document
#: brochure.en.tex:293
msgid ""
"DebConf helps share the knowledge and experience of individual developers "
"with the entire community. It also helps promote enthusiasm for specialized "
"areas of development, and encourages \\textbf{discussions about Debian's "
"future}."
msgstr ""

#. type: DCquote{#1}
#: brochure.en.tex:303
msgid ""
"At DebConf3 in Oslo, I finally met the other Debian Installer developers "
"gathered together in person, and after a week of challenging work, we "
"achieved the first successful installation of Debian with it.  This "
"reinvigorated our team, leading to many new members, more rapid development, "
"and many more developer gatherings.  The resulting program has since been "
"used to install Debian, and derivative distributions, on tens of millions of "
"systems."
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:305
msgid "Work Groups"
msgstr ""

#. type: textbf{#1}
#: brochure.en.tex:310
msgid ""
"DebConf enables some of the most productive sessions that Debian has "
"experienced"
msgstr ""

#. type: document
#: brochure.en.tex:310
msgid ""
". To have everyone in one place, at the same time, working on the same "
"system allows people to bounce ideas off each other instantly."
msgstr ""

#.  <http://anonscm.debian.org/viewvc/publicity/announcements/en/drafts/biggest-debconf-achievement.txt?view=markup&pathrev=3229>
#. type: DCquote{#1}
#: brochure.en.tex:318
msgid ""
"During DebConf5 we discussed the idea of a packaging team for Perl "
"libraries; together with the GNOME team this effort was pioneering the idea "
"of collaborative package maintenance that proved to be very effective."
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:320
msgid "Project Synergy"
msgstr ""

#. type: document
#: brochure.en.tex:326
msgid ""
"DebConf gives contributors a chance to meet face to face, socialize, and "
"brainstorm new ideas. As a result people leave DebConf with a renewed sense "
"of enthusiasm for Debian and its goals. \\textbf{The latest ideas and "
"systems that Debian implements often start as an informal session at "
"DebConf}."
msgstr ""

#. type: DCquote{#1}
#: brochure.en.tex:333
msgid ""
"As Debian Account Manager, DebConf is a priceless opportunity for me to "
"handle sensitive matters face to face, in an appropriate setting.  Over time "
"there will be several such issues accumulating that cannot easily be handled "
"via email."
msgstr ""

#. type: section{#2}
#: brochure.en.tex:335
msgid "Benefits"
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:337
msgid "Sponsorship Levels"
msgstr ""

#. type: document
#: brochure.en.tex:372
msgid ""
"{ \\small\\tabulinesep=4pt \\begin{tabu} to \\textwidth{@{}X[1] X[-1,c] "
"X[-1,c] X[-1,c] X[-1,c] X[-1,c] } "
"\\rowfont{\\bfseries\\color{DebConf_tableheader}} Description & Supporter & "
"Bronze & Silver & Gold & Platinum \\\\ Logo on the sponsor web page & "
"$\\surd$ & $\\surd$ & $\\surd$ & $\\surd$ & $\\surd$ \\\\ Logo on all web "
"pages and link back to company homepage & & $\\surd$ & $\\surd$ & $\\surd$ & "
"$\\surd$ \\\\ Logo in full-page ``thank you'' ad in Linux Magazine worldwide "
"& & & $\\surd$ & $\\surd$ & $\\surd$ \\\\ Logo printed on conference "
"T-Shirts & & & $\\surd$ & $\\surd$ & $\\surd$ \\\\ Logo printed larger and "
"in superior position & & & & $\\surd$ & $\\surd$ \\\\ Logo on all video "
"streams, during breaks & & & & $\\surd$ & $\\surd$ \\\\ Logo on banner in "
"conference lobby & & & & $\\surd$ & $\\surd$ \\\\ Logo on banner behind talk "
"podiums & & & & & $\\surd$ \\\\ Name and description of sponsor in all press "
"releases relating to the conference & & & & & $\\surd$ \\\\ 45-minute slot "
"for a Free Software-related talk & & & & & $\\surd$ \\\\ "
"\\rowfont{\\bfseries\\color{DebConf_tableheader}} Contribution in CHF & "
"<2~000 & 2~000 & 6~000 & 12~000 & 25~000 \\end{tabu} }"
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:372
msgid "Additional Opportunities"
msgstr ""

#. type: document
#: brochure.en.tex:377
msgid ""
"We also invite sponsors, in addition to their basic sponsorship, to sponsor "
"a specific part of the conference.  Here are some of the opportunities "
"available -- please contact the sponsorship team to discuss prices and other "
"details:"
msgstr ""

#. type: itemize
#: brochure.en.tex:390
msgid "Hardware infrastructure"
msgstr ""

#. type: itemize
#: brochure.en.tex:390
msgid "Lecture and meeting rooms"
msgstr ""

#. type: itemize
#: brochure.en.tex:390
msgid "Conference dinner"
msgstr ""

#. type: itemize
#: brochure.en.tex:390
msgid "Attendee travel bursaries"
msgstr ""

#. type: itemize
#: brochure.en.tex:390
msgid "Sponsored meals for attendees"
msgstr ""

#. type: itemize
#: brochure.en.tex:390
msgid "Sponsored accommodation for attendees"
msgstr ""

#. type: itemize
#: brochure.en.tex:390
msgid "Coffee or snacks"
msgstr ""

#. type: itemize
#: brochure.en.tex:390
msgid "Day trip (social event for the whole group, without computers)"
msgstr ""

#. type: section{#2}
#: brochure.en.tex:392
msgid "Reasons"
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:394
msgid "In the Words of Our Past Sponsors"
msgstr ""

#. type: document
#: brochure.en.tex:398
msgid ""
"There are as many reasons to sponsor DebConf as there are reasons Debian "
"developers spend their time to fix bugs, improve the overall system, and "
"help its installed base. In the words of our past sponsors:"
msgstr ""

#. type: DCquote{#1}
#: brochure.en.tex:406
msgid ""
"Sponsoring DebConf has benefited Bytemark in two ways: 1) it puts our brand "
"in front of exactly the right crowd and 2) it allows Steve, the Debian "
"developer on our staff, to keep in touch with the project that we rely on so "
"heavily. We support DebConf because it supports us."
msgstr ""

#. type: DCquote{#1}
#: brochure.en.tex:414
msgid ""
"Sponsoring DebConf helps us to get the recognition we need from inside the "
"Debian Project. It is astonishing how many potential employees, partners and "
"even customers are aware of our sponsorship activities at DebConf. We even "
"got a couple of exciting job applications only through DebConf."
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:416
msgid "The Value of Sponsorship"
msgstr ""

#. type: document
#: brochure.en.tex:425
msgid ""
"Many of DebConf's high profile attendees are well known around the world and "
"their opinions and views are followed by many individuals and "
"businesses. Debian is a democracy of its developers where smaller voices can "
"be easily heard. Participating through sponsorship is one of the many ways "
"to help the project achieve its full potential. The knowledge that Debian, "
"its community, and what it represents, is of a certain value to companies "
"and individuals is the best show of appreciation to the volunteer efforts of "
"many Debian members."
msgstr ""

#.  Kevin: add examples from the DebConf12 survey
#. type: document
#: brochure.en.tex:430
msgid ""
"A modest amount of sponsorship would not only help Debian, but would earn "
"you goodwill in the Free Software community at large. Past DebConfs have "
"also offered great opportunities for contact building and recruitment."
msgstr ""

#. type: subsection{#2}
#: brochure.en.tex:433
msgid "The Need for Sponsorship"
msgstr ""

#. type: document
#: brochure.en.tex:436
msgid ""
"The Debian Project does not engage in commercial activities and depends "
"solely on donations from users and well-wishers."
msgstr ""

#.  <mid:CAEwy9bg3L8EAQDqq7ZY=eJiF6ducupEjDQd1wbSJDLpCSpaEvQ@mail.gmail.com>
#.  <http://lists.debconf.org/lurker/message/20121005.100049.4bb6b521.en.html>
#.  <http://wiki.debconf.org/wiki/DebConf13/Switzerland/Bid#Rough_budget_calculations>
#. type: document
#: brochure.en.tex:445
msgid ""
"DebConf has evolved from humble beginnings in Bordeaux, France (2000)  with "
"30 attendees and virtually no budget, to recent years where we accommodate "
"an average of 250 attendees, with a budget of around 210~000.00 CHF.  "
"DebConf13 is a challenge, as Switzerland is a demanding destination in terms "
"of cost."
msgstr ""

#. type: document
#: brochure.en.tex:449
msgid ""
"DebConf organizational teams are made up of volunteers to keep costs at a "
"minimum. However, as we expect a turnout of 300 attendees, there is a real "
"need for substantial sponsoring."
msgstr ""

#. type: textbf{#1}
#: brochure.en.tex:452
msgid ""
"Sponsorship will be paid to the ``DebConf13 association'', a non-profit "
"association"
msgstr ""

#. type: document
#: brochure.en.tex:452
msgid "under Swiss law."
msgstr ""

#. type: section{#2}
#: brochure.en.tex:454
msgid "More About DebConf"
msgstr ""

#. type: document
#: brochure.en.tex:458
msgid ""
"For more information about DebConf, the final reports from our past "
"conferences illustrate the broad spectrum, quality, and enthusiasm of the "
"community at work: \\url{http://media.debconf.org/reports/}."
msgstr ""

#. type: document
#: brochure.en.tex:462
msgid ""
"For further details, feel free to contact us through "
"\\href{mailto:sponsors@debconf.org}{sponsors@debconf.org} or visit our "
"website at \\url{http://debconf13.debconf.org}"
msgstr ""

#. type: document
#: brochure.en.tex:464
msgid "Thank you for reading this proposal. We hope to hear from you soon!"
msgstr ""

#. type: addcontentsline{#3}
#: brochure.en.tex:476
msgid "See you soon!"
msgstr ""

#. type: textbf{#1}
#: brochure.en.tex:476
msgid "See You in Vaumarcus!"
msgstr ""

#. type: Plain text
#: brochure.en.tex:483
msgid "<!-- Local Variables: ispell-dictionary: \"english\" End: -->"
msgstr ""
