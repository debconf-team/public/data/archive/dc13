\documentclass[a4paper,11pt]{scrartcl}

\usepackage[
    pdfauthor={DebConf13 association},
    pdftitle={ DebConf13 association Bylaws},
    hidelinks
]{hyperref}

% Standard margins; left is bigger because titles are "-1 cm"
\usepackage[top=2cm,bottom=2cm,left=2.5cm,right=1.5cm]{geometry}
\newlength{\titlesmargin}
\setlength{\titlesmargin}{-1cm}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fancyhdr}

%% Bottom shade
\usepackage[all]{background}
\usepackage{tikz}

%% Coloured titles
\usepackage{titlesec}
\usepackage{color}

\definecolor{DebConf_Section}{RGB}{213, 43, 30}
\definecolor{DebConf_SubSection}{RGB}{213, 43, 30}
\definecolor{DebConf_tableheader}{RGB}{213, 43, 30}
\definecolor{dcshade}{RGB}{213, 43, 30}

\usepackage{multicol}

% Continuous Article (subsection) numbering
\usepackage{chngcntr}
\counterwithout{subsection}{section}

\renewcommand{\thesubsection}{\arabic{subsection}}

\titleformat{\section}{\color{DebConf_Section}\sffamily\Huge\bfseries}{}{0pt}{}
\titlespacing*{\section}{\titlesmargin}{*2}{*1.5}
\titleformat{\subsection}{\color{DebConf_SubSection}\sffamily\Large\bfseries}{Article \thesubsection. }{0pt}{}
\titlespacing*{\subsection}{\titlesmargin}{*2}{*1.5}

\newcommand{\article}[1]{\subsection{#1}}

\setlength{\headheight}{1cm}
\setlength{\footskip}{1.2cm}
\setlength{\headsep}{0cm}
\setlength{\textheight}{25cm}

\newcommand{\DCShade}{
\begin{tikzpicture}[remember picture,overlay]
    \shade[top color=white,bottom color=dcshade] (0,-\paperheight) rectangle (\paperwidth,-\paperheight+2.7cm);
 \end{tikzpicture}
}

\SetBgContents{\DCShade}
\SetBgPosition{current page.north west}
\SetBgOpacity{1.0}
\SetBgAngle{0.0}
\SetBgScale{1.0}

\pagestyle{fancy}
\lhead{~}
\chead{~}
\rhead{~}

\lfoot{~}
\cfoot{~}
\rfoot{\color{white}\bfseries~\\DebConf13 association Bylaws}

\renewcommand{\headrulewidth}{0pt}

% For the GPG signature
\newcommand{\dontcompile}[2]{#2}

\title{DebConf13 association}
\subtitle{Bylaws}
\date{~}

\begin{document}

\maketitle
\thispagestyle{fancy}

\dontcompile {
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

% ==== BEGIN BYLAWS ====
} % Mandatory to hide the GPG signature

\newcommand{\Board}{``Board'' }
\newcommand{\GeneralMeeting}{``General Meeting'' }

\section{Generalities}

\article{Name}

``DebConf13 association'' is a non-profit association, governed by the present bylaws and, secondarily, by Articles 60 et seq. of the Swiss Civil Code.
It is politically neutral and non-denominational.

\article{Aim}

The association shall pursue the following aims:
\begin{itemize}
\item Organise the annual Debian Conference 2013 (DebConf13) in Switzerland;
\item Support the Debian project.
\end{itemize}

\article{Seat}

The legal seat of the association is located where the treasurer (as member of the \Board, elected by the \GeneralMeeting of members) legally resides.
If required the seat can be changed by a \Board decision.

\article{Relation to Debian and DebConf}

The Debian Conference is organized for the Debian Project and in cooperation with its DebConf Chairs and DebConf Team.
The association does not take any major decision without consultation with the DebConf Chairs and the DebConf Team.

\section{Members}

\article{Membership}

Any physical person may request to join if they are willing to pursue the goals of the association.
Requests to become a member must be addressed to the \Board.
The \Board admits new members and informs the \GeneralMeeting accordingly.

\begin{itemize}
\item A quarter of the existing members can appeal any membership decision;
\item The appeal is ruled upon by the \GeneralMeeting but has no suspensive effect on the membership decision by the \Board, either way.
\end{itemize}

\article{Ceasing}

Membership ceases:
\begin{itemize}
\item on death;
\item by resignation;
\item by exclusion.
\end{itemize}
Resignation becomes effective as soon as properly communicated to the \Board.

The exclusion of a member is a decision of the \Board for a member who, through her attitude, behavior or actions, has put herself in contradiction with the goals of the association or for any cause determined to be of sufficient gravity by the \Board.

The exclusion decision can be appealed to the \GeneralMeeting but has no suspensive effect.
The \GeneralMeeting doesn't have to justify of its final decision.

\section{Resources and finances}

\article{Subscription fees}

The members of the association have no duty to pay subscriptions.

\article{Resources}

The association's resources come from:
\begin{itemize}
\item donations;
\item legacies;
\item private and public subsidies;
\item any other resources allowed by law.
\end{itemize}

The funds shall be used in conformity with the association's aims.

\article{Liability}

The members of the association have no individual financial liability and the liability of the association is limited to its assets only.
The members of the association have no right on the assets of the association.

\section{Bodies}

\article{Bodies}

The association's bodies are:
\begin{itemize}
\item the \GeneralMeeting of members;
\item the \Board.
\end{itemize}

\section{General Meeting (GM)}

\article{GM - Composition}

The \GeneralMeeting of members is constituted by all the members of the association.

\article{GM - Powers}

The \GeneralMeeting, in addition to powers attributed by law:
\begin{itemize}
\item appoints the members of the \Board;
\item shall rule upon appeals regarding admission and expulsion of members;
\item decides on any modification of the present bylaws;
\item decides on the dissolution of the association.
\end{itemize}

\article{GM - Decision-making}

Decisions of the \GeneralMeeting of members shall be taken by a majority vote of the members present.
In case of deadlock, the President shall have the deciding vote.

Decisions concerning the amendment of the bylaws and the dissolution of the association must be approved by a two-third majority of the members present.

\section{Board}

\article{Board - Composition}

The \Board is constituted of members of the association, elected in their functions by the \GeneralMeeting:
\begin{itemize}
 \item president
 \item treasurer
 \item secretary
 \item non-executive members (facultative)
\end{itemize}

\article{Board - Attributions}

The \Board, in addition to obligations defined by law, has the responsibility of:
\begin{itemize}
\item conducting the activities of the association in pursuit of its goals;
\item the finances of the association;
\item deciding upon membership requests;
\end{itemize}

The "Board" has to agree with a two-third majority and the ''DebConf Team'' has to be consulted before deciding:
\begin{itemize}
\item upon any expense above 1'000 CHF;
\item to sign contracts in the name of the association, if the contract implies expenses above 1'000 CHF.
\end{itemize}

The President, the Treasurer and the Secretary are each entitled to sign contracts in the name of the association jointly together with at least one other member of the "Board".

\section{Final considerations}

\article{Representation}

The \Board is entitled and obliged to manage and represent the association.

\article{Commercial register entry}

The association is not registered to the commercial register.

\article{Accounting year}

The accounting year is the calendar year; the first accounting year ends on 31. December 2013.

\article{Assets in case of dissolution}

In the case of the association being dissolved, the assets should be allotted to a non-profit organisation holding assets in trust for Debian or any organisation responsible for a future DebConf.
The goods of the association cannot be returned to the founders or members, nor be used to their own profit.
The decision has to be taken by a two-third majority of the present members.

\dontcompile { % Mandatory to hide the GPG signature
% ==== END BYLAWS ====
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQGcBAEBCAAGBQJQUxGpAAoJEIvPpx7KFjRVNMwMAInWepo8JpK0QX/AHHEMxy7E
BQQy18gkil4QbJUjgVGgOMcDNAGRwGAc0sxYM66UObu8myzlE6FGJ5zqe7/3GgJ+
1quqgOvxsn2ULwYr9nCz5atCRBgFjUq1dbwmK/PFZReBQLs4FIsq6JF/hap6CKes
9S/G2QAdY66G+8R28EsdsFN3uhGvo5+caq2NnQFcKbbJdswUJmTVjCkEttniWMvg
VfoAJggvXbIxCKJI6Kw8YPpJtGoHx+40XL+ikP3jSSiLllpdGmiy6ORGHXSf24ZG
Wa7RGkphz0RYslgPcO2XE2ZRT9JtGw1HMuhxgIY9UT2Rj3vpAlqv2wUY2PP1ALAE
Z/Zj/p3TB62gLEAxKTUHYD8Cta6ZY8/X5zsjXkBIHCMXIzS6zVx7OpXbrOfEA59x
rrYBDwmQQFwgyGOOHSSbiAbYkGIl9+N0tRmHdvnC8qWUM/gCt4Ed6orfJjYFSRcQ
MuE50vZlK4GkkHH5jgE4caZX84N4p9fQqegBzSyU4A==
=l4Vf
-----END PGP SIGNATURE-----
} % Mandatory to hide the GPG signature

% == Approval signatures ==

\section{Signatures}
The present bylaws have been approved by the Founding \GeneralMeeting (FGM) on September 5. 2012 in Bern. In name of the association:
\begin{multicols}{2}
\centering
~\\The FGM \textit{Chairman} and\\\textit{non-executive member of the Board} of the association
~\\~\\Gaudenz Steinlin

~\\The FGM \textit{Recording Secretary} and\\ \textit{Secretary} of the association
~\\~\\Didier Raboud
\end{multicols}

\end{document}
